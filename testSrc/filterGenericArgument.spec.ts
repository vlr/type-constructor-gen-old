import { filterGenericArgument } from "../src/produce/filterGenericArgument";
import { expect } from "chai";

describe("filterGenericArgument", function (): void {
  it("should filter Generic Argument", function(): void {
    // arrange
    const name = "CompanyId<T>";
    const expected = "CompanyId";

    // act
    const result =  filterGenericArgument(name);

    // assert
    expect(result).equals(expected);
  });

  it("should not touch non Generic name", function(): void {
    // arrange
    const name = "CompanyId";
    const expected = "CompanyId";

    // act
    const result =  filterGenericArgument(name);

    // assert
    expect(result).equals(expected);
  });

});
