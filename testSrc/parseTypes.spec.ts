import { ParsedType } from "../src/parse/types";
import { parseTypes } from "../src";
import { expect } from "chai";

describe("parseTypes", function (): void {
  it("should parse an interface", function (): void {
    // arrange
    const src = `
export interface MyType {
  name: string;
}
`;
    const expected: ParsedType[] = [
      {
        name: "MyType",
        fields: [
          {
            name: "name",
            isOptional: false,
            type: "string"
          }
        ]
      }
    ];

    // act
    const result = parseTypes(src);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should parse readonly type", function (): void {
    // arrange
    const src = `
export type MyType = Readonly<{
  name: string;
}>;
`;
    const expected: ParsedType[] = [
      {
        name: "MyType",
        fields: [
          {
            name: "name",
            isOptional: false,
            type: "string"
          }
        ]
      }
    ];

    // act
    const result = parseTypes(src);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should parse an interface with several fields", function (): void {
    // arrange
    const src = `
export interface MyType {
  val: string;
  val2: number;
  complex: Type<string, number[]>[]
}
`;
    const expected: ParsedType[] = [
      {
        name: "MyType",
        fields: [
          {
            name: "val",
            isOptional: false,
            type: "string"
          },
          {
            name: "val2",
            isOptional: false,
            type: "number"
          },
          {
            name: "complex",
            isOptional: false,
            type: "Type<string, number[]>[]"
          }
        ]
      }
    ];

    // act
    const result = parseTypes(src);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should parse an interface with optional field", function (): void {
    // arrange
    const src = `
export interface MyType {
  val?: string;
}
`;
    const expected: ParsedType[] = [
      {
        name: "MyType",
        fields: [
          {
            name: "val",
            isOptional: true,
            type: "string"
          }
        ]
      }
    ];

    // act
    const result = parseTypes(src);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should ignore extended interface ", function(): void {
    // arrange
    const src = `
export interface MyType extends Smth {
  val?: string;
}
`;

    // act
    const result = parseTypes(src);

    // assert
    expect(result).deep.equals([]);
  });
});
