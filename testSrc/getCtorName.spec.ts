import { getCtorName } from "../src/produce/getCtorName";
import { expect } from "chai";

describe("getCtorName", function (): void {
  it("should lowercase first letter", function(): void {
    // arrange
    const name = "CompanyId";
    const expected = "companyId";

    // act
    const result = getCtorName(name);

    // assert
    expect(result).equals(expected);
  });

  it("should add `create` if first letter already lowercase", function(): void {
    // arrange
    const name = "companyId";
    const expected = "createCompanyId";

    // act
    const result = getCtorName(name);

    // assert
    expect(result).equals(expected);
  });
});
