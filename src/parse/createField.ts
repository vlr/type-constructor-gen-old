import { ParsedField, parsedField } from "./types";

export function createField(name: string, type: string): ParsedField {
  if (isEmpty(name) || isEmpty(type)) {
    return null;
  }
  name = name.trim();
  const isOptional = name.endsWith("?");
  if (isOptional) { name = name.slice(0, name.length - 1); }
  return parsedField(name, type.trim(), isOptional);
}

function isEmpty(str: string): boolean {
  return str == null || str.trim().length === 0;
}
