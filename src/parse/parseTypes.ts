import { regexToArray } from "@vlr/array-tools";
import { parseTypeContents } from "./parseTypeContents";
import { ParsedType } from "./types";

export function parseTypes(content: string): ParsedType[] {
  return parseInterfaces(content)
    .concat(parseReadonlyTypes(content)).filter(x => x != null);
}

const interfaceRegex = /export\s*interface\s*(.*)\s*?{\r?\n((?:.*?\r?\n?)*?)\s*?}/g;
function parseInterfaces(content: string): ParsedType[] {
  const matches = regexToArray(interfaceRegex, content);
  return matches.map(match => parseTypeContents(match[1], match[2]));
}

const typeRegex = /\s*?export\s*type\s*([^\s]*)\s*?=\s*?(?:Recursive)?Readonly<{\s*?\r?\n((?:.*?\r?\n)*?)\s*?}>;/g;
function parseReadonlyTypes(content: string): ParsedType[] {
  const matches = regexToArray(typeRegex, content);
  return matches.map(match => parseTypeContents(match[1], match[2])).filter(x => x);
}


