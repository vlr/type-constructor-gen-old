
export interface ParsedField {
  name: string;
  type: string;
  isOptional: boolean;
}
