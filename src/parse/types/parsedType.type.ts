import { ParsedField } from "./parsedField.type";

export interface ParsedType {
  name: string;
  fields: ParsedField[];
}


