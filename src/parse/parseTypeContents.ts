import { regexToArray } from "@vlr/array-tools";
import { createField } from "./createField";
import { ParsedType, parsedType } from "./types";

export const fieldRegex = /\s*(.*)\s*:\s*([^;]*);?/g;
export function parseTypeContents(name: string, content: string): ParsedType {
  name = name.trim();
  if (name.match(/\s/) || !name.length) {
    return null;
  }

  const matches = regexToArray(fieldRegex, content);
  const fields = matches.map(match => createField(match[1], match[2]));
  if (fields.some(p => !p)) {
    console.log(`Type, name: ${name} is ignored, can't parse fields`);
    return null;
  }
  if (!fields.length) {
    console.log(`Type, name: ${name} is ignored, no fields found`);
    return null;
  }
  return parsedType(name, fields);
}
