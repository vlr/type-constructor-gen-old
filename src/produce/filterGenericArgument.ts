export function filterGenericArgument(name: string): string {
  return name.replace(/<.*>/g, "");
}
