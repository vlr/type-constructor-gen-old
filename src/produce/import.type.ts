export { Import } from "@vlr/imports-gen";
import { Import } from "@vlr/imports-gen";

export type ImportsMap = Map<string, Import>;
