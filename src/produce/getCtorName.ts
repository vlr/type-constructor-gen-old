export function getCtorName(name: string): string {
  const firstLetter = name[0];
  const lower = firstLetter.toLowerCase();
  if (firstLetter === lower) {
    return replaceFirstLetter(name, "create" + firstLetter.toUpperCase());
  }
  return replaceFirstLetter(name, lower);
}

function replaceFirstLetter(name: string, replacement: string): string {
  return name.replace(/^./, replacement);
}

export function getCamelName(name: string): string {
  const firstLetter = name[0];
  return replaceFirstLetter(name, firstLetter.toLowerCase());
}
