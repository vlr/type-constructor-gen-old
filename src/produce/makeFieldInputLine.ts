import { FieldModel } from "./types";

export function makeFieldInputLine(fields: FieldModel[]): string {
  return fields.map(makeFieldInput).join(", ");
}

export function makeFieldInput(field: FieldModel): string {
  return `${field.name}: ${field.type}`;
  //  field.isOptional
  //   ? `${field.name}?: ${field.type}`
  //   : `${field.name}: ${field.type}`;
}
