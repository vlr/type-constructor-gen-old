import { ParsedType, Import } from "../parse/types";
import { GeneratorOptions } from "../generatorOptions.type";
import { methods } from "./generators/methods";
import { filterImports } from "./filterImports";
import { convertTypes } from "./convertTypes";
import { libMark } from "./libMark";
import { methodsModel, MethodsModel } from "./types";
import { filterGenericArgument } from "./filterGenericArgument";

export function produceMethods(imports: Import[], types: ParsedType[], fileName: string, options: GeneratorOptions): string {
  const model = createModel(imports, types, fileName, options);
  return methods.generate(model, options);
}

function createModel(imports: Import[], types: ParsedType[], fileName: string, options: GeneratorOptions): MethodsModel {
  const resultImports = filterImports(imports, types).concat(getImportsForTypes(types, fileName));
  return methodsModel(libMark(options), resultImports, convertTypes(types));
}

function getImportsForTypes(types: ParsedType[], fileName: string): Import[] {
  return types.map(type => getImportForType(type, fileName));
}

function getImportForType(type: ParsedType, fileName: string): Import {
  const typeName = filterGenericArgument(type.name);
  return { type: typeName, alias: typeName, importPath: `./${fileName}` };
}


