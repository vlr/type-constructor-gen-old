// warning! This code was generated by @vlr/razor
// manual changes to this file will be overwritten next time the code is regenerated.

import { Generator, GeneratorOptions } from "@vlr/razor/export";
import { TypeModel } from "../types";

function generateContent(type: TypeModel, gen: Generator): Generator {
  const indent = gen.indent;
  const fieldName = type.fields[0].name;
  gen = gen.append(`export function `);
  gen = gen.append((type.ctorName).toString());
  gen = gen.append(`(id: `);
  gen = gen.append((type.fields[0].type).toString());
  gen = gen.append(`): `);
  gen = gen.append((type.name).toString());
  gen = gen.append(` {`);
  gen = gen.eol();
  gen = gen.append(`  return `);
  gen = gen.append((fieldName).toString());
  gen = gen.append(` == null ? null : { `);
  gen = gen.append((fieldName).toString());
  gen = gen.append(`: id };`);
  gen = gen.eol();
  gen = gen.append(`}`);
  gen = gen.eol();
  gen = gen.eol();
  gen = gen.append(`export function is`);
  gen = gen.append((type.name).toString());
  gen = gen.append(`(obj: any): obj is `);
  gen = gen.append((type.name).toString());
  gen = gen.append(` {`);
  gen = gen.eol();
  gen = gen.append(`  return obj !== null && typeof obj === `);
  gen = gen.quote();
  gen = gen.append(`object`);
  gen = gen.quote();
  gen = gen.append(` && `);
  gen = gen.quote();
  gen = gen.append((fieldName).toString());
  gen = gen.quote();
  gen = gen.append(` in obj;`);
  gen = gen.eol();
  gen = gen.append(`}`);
  gen = gen.eol();
  gen = gen.eol();
  gen = gen.append(`export function `);
  gen = gen.append((type.ctorName).toString());
  gen = gen.append(`Equals(id1: `);
  gen = gen.append((type.name).toString());
  gen = gen.append(`, id2: `);
  gen = gen.append((type.name).toString());
  gen = gen.append(`): boolean {`);
  gen = gen.eol();
  gen = gen.append(`  return (id1 && id1.`);
  gen = gen.append((fieldName).toString());
  gen = gen.append(`) === (id2 && id2.`);
  gen = gen.append((fieldName).toString());
  gen = gen.append(`);`);
  gen = gen.eol();
  gen = gen.append(`}`);
  gen = gen.eol();
  gen = gen.setIndent(indent);
  return gen;
}

function generate(type: TypeModel, options: GeneratorOptions): string {
  return generateContent(type, new Generator(options)).toString();
}

export const idMethods = {
  generate,
  generateContent
};
