import { flatMap, unique } from "@vlr/array-tools";
import { toMap } from "@vlr/map-tools";
import { Import, ImportsMap, ParsedType } from "../parse/types";

export function filterImports(imports: Import[], types: ParsedType[]): Import[] {
  const map = toMap(imports, imp => imp.alias);
  return unique(flatMap(types, type => getTypeImports(type, map)));
}

function getTypeImports(type: ParsedType, map: ImportsMap): Import[] {
  return flatMap(type.fields, field => getTypeNameImports(field.type, map));
}

const splitter = /[<>,\s\[\]]/;
function getTypeNameImports(typeName: string, imports: ImportsMap): Import[] {
  const parts = typeName.split(splitter)
    .map(part => part.trim());
  return unique(parts)
    .filter(part => imports.has(part))
    .map(part => imports.get(part));
}
