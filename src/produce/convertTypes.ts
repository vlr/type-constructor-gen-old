import { ParsedType } from "../parse/types";
import { getCtorName, getCamelName } from "./getCtorName";
import { typeModel, TypeModel } from "./types";

export function convertTypes(types: ParsedType[]): TypeModel[] {
  return types.map(convertType);
}

function convertType(type: ParsedType): TypeModel {
  return typeModel(type.name, getCtorName(type.name), isTypeId(type), type.fields);
}

function isTypeId(type: ParsedType): boolean {
  return type.fields.length === 1
    && (type.fields[0].type === "string" || type.fields[0].type === "number")
    && getCamelName(type.name) === getCamelName(type.fields[0].name);
}
