import { Import } from "../../parse/types";

export interface MethodsModel {
  libMark: string;
  imports: Import[];
  types: TypeModel[];
}

export interface TypeModel {
  name: string;
  ctorName: string;
  isId: boolean;
  fields: FieldModel[];
}

export interface FieldModel {
  name: string;
  type: string;
  isOptional: boolean;
}
