import { GeneratorOptions } from "@vlr/razor/export";
import { parseTypes } from "./parse";
import { produceMethods } from "./produce/produceMethods";
import { parseImports } from "@vlr/imports-gen";

export function generate(content: string, fileName: string, options: GeneratorOptions): string {
  const types = parseTypes(content);
  if (types.length === 0) { return null; }
  const imports = parseImports(content);
  const result = produceMethods(imports, types, fileName, options);
  return result;
}
