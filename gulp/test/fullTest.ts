import { series } from "gulp";
import { runTestCoverage, runTestsEs5, runTestsEs6 } from "./parts";
import { fullBuild } from "../build/fullBuild";

export const fullTest = series(
  fullBuild,
  runTestsEs5, runTestsEs6,
  runTestCoverage,
);
