import * as fse from "fs-extra";
import { retryPromise } from "./retryPromise";

export function removeDir(path: string): Promise<void> {
  return retryPromise(async () => {
    const exists = await fse.pathExists(path);
    if (exists) {
      await fse.remove(path);
    }
  });
}
