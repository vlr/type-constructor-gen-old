import { parallel, series } from "gulp";
import { automate } from "../automate/automate";
import { removeBuildFolder, tslint } from "./parts";

export const preparation = parallel(removeBuildFolder, series(automate, tslint));
