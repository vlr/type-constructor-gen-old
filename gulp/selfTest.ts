import * as gulp from "gulp";
import { transform, changeFileType } from "@vlr/gulp-transform";

export function selfTest(): any {
  const options = {
    quotes: "\"",
    lineFeed: "\n"
  };
  // tslint:disable-next-line:no-require-imports
  const target = require("../build/src");
  return gulp.src("./src/**/*.type.ts")
    .pipe(transform(file => ({
      ...file,
      contents: target.generate(file.contents, file.name, options),
      name: changeFileType(file.name, ".ctor")
    })))
    .pipe(gulp.dest("./src"));
}
